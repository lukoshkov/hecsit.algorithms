﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hecsit.Algorithms.Lab2.Test
{
    [TestClass]
    public class KBasedNumberFixture
	{
		[TestMethod]
		public void Recursive_Base_2_Length_1_Gives_1()
		{
			// 1
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(1);
			ulong expected = 1;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_2_Length_2_Gives_2()
		{
			// 10, 11
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(2);
			ulong expected = 2;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_2_Length_3_Gives_3()
		{
			// 101, 110, 111
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(3);
			ulong expected = 3;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_2_Length_4_Gives_5()
		{
			// 1010, 1011, 1101, 1110, 1111
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(4);
			ulong expected = 5;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_2_Length_5_Gives_8()
		{
			// 10101, 10110, 10111, 11010, 11011, 11101, 11110, 11111
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(5);
			ulong expected = 8;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_2_Length_6_Gives_13()
		{
			// 101010, 101011, 101101, 101110, 101111, 110101, 110110, 110111, 111010, 111011, 111101, 111110, 111111
			var number = new KBasedNumber(2);
			var actual = number.GetNDigitNumbersQuantityRecursive(6);
			ulong expected = 13;
			Assert.AreEqual(actual, expected);
		}

        [TestMethod]
        public void Recursive_Base_3_Length_1_Gives_2()
		{
			// 1, 2
			var number = new KBasedNumber(3);
			var actual = number.GetNDigitNumbersQuantityRecursive(1);
			ulong expected = 2;
			Assert.AreEqual(actual, expected);
        }

		[TestMethod]
		public void Recursive_Base_3_Length_2_Gives_6()
		{
			// 10, 11, 12, 20, 21, 22
			var number = new KBasedNumber(3);
			var actual = number.GetNDigitNumbersQuantityRecursive(2);
			ulong expected = 6;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_3_Length_3_Gives_16()
		{
			// 101, 102, 110, 111, 112, 120, 121, 122, 201, 202, 210, 211, 212, 220, 221, 222
			var number = new KBasedNumber(3);
			var actual = number.GetNDigitNumbersQuantityRecursive(3);
			ulong expected = 16;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Recursive_Base_3_Length_4_Gives_44()
		{
			// 1010,1011,1012; 1020,1021,1022; 1101,1102; 1110,1111{10},1112; 1120,1121,1122; 1201,1202; 1210,1211,1212; 1220{20},1221,1222; 2010,2011,2012; 2020,2021,2022; 2101,2102{30}; 2110,2111,2112; 2120,2121,2122; 2201,2202; 2210,2211{40},2212; 2220,2221,2222.
			var number = new KBasedNumber(3);
			var actual = number.GetNDigitNumbersQuantityRecursive(4);
			ulong expected = 44;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public void Iterative_Base_3_Length_4_Gives_44()
		{
			// 1010,1011,1012; 1020,1021,1022; 1101,1102; 1110,1111{10},1112; 1120,1121,1122; 1201,1202; 1210,1211,1212; 1220{20},1221,1222; 2010,2011,2012; 2020,2021,2022; 2101,2102{30}; 2110,2111,2112; 2120,2121,2122; 2201,2202; 2210,2211{40},2212; 2220,2221,2222.
			var number = new KBasedNumber(3);
			var actual = number.GetNDigitNumbersQuantityIterative(4);
			ulong expected = 44;
			Assert.AreEqual(actual, expected);
		}
    }
}
