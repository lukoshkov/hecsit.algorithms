﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab5.Benchmarkable
{
	public class KeysWithoutDuplication
	{
		private Random _random;

		public KeysWithoutDuplication(Random random)
		{
			_random = random;
		}

		public int[] Generate(int size)
		{
			var max = int.MaxValue / size;
			var result = new int[size];
			result[0] = _random.Next(0, max);
			for (int i = 1; i < size; i++)
			{
				result[i] = result[i - 1] + _random.Next(1, max);
			}
			return Rearrange(result);
		}

		private int[] Rearrange(int[] array)
		{
			for (int i = 0; i < array.Length / 2; i++)
			{
				int first = _random.Next(0, array.Length - 1);
				int second = _random.Next(0, array.Length - 1);
				var buffer = array[first];
				array[first] = array[second];
				array[second] = buffer;
			}
			return array;
		}
	}
}
