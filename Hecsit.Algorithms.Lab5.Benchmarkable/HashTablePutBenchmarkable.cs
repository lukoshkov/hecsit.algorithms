﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms.Lab5.Core;

namespace Hecsit.Algorithms.Lab5.Benchmarkable
{
	public class HashTablePutBenchmarkable : IBenchmarkable
	{
		private HashTable<int, string> _dictionary;
		private readonly Random _random;
		private readonly int _initCapacity;
		private readonly Func<long, double> _sizeToLoadFactor;
		private int[] _keys;
		private readonly int _getsCount;

		public HashTablePutBenchmarkable(Random random, int initCapacity, int getsCount, Func<long, double> sizeToLoadFactor)
		{
			_random = random;
			_initCapacity = PrimeNumber.Next(initCapacity - 1);
			_sizeToLoadFactor = sizeToLoadFactor;
			_getsCount = getsCount;
		}

		public void Init(long size)
		{
			Console.Write("Initializing (HT Put) load factor {0}... ", _sizeToLoadFactor(size));
			_dictionary = new HashTable<int, string>(_initCapacity);
			var initCount = (int)(_sizeToLoadFactor(size) * _initCapacity);
			_keys = new KeysWithoutDuplication(_random).Generate(initCount + _getsCount);
			var text = "init string";
			for (var i = 0; i < initCount; i++)
			{
				_dictionary.Put(_keys[i], text);
			}
			Console.WriteLine("Testing...");
		}

		public void Execute()
		{
			for (int i = _keys.Length - _getsCount; i < _keys.Length; i++)
			{
				_dictionary.Put(_keys[i], "test string");
			}
		}
	}
}
