﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms.Lab5.Core;

namespace Hecsit.Algorithms.Lab5.Benchmarkable
{
	public class DictionaryGetValueBenchmarkable : IBenchmarkable
	{
		private Dictionary<int, string> _dictionary;
		private readonly Random _random;
		private readonly int _initCapacity;
		private readonly int _getsCount;
		private readonly Func<long, double> _sizeToLoadFactor;

		public DictionaryGetValueBenchmarkable(Random random, int initCapacity, int getsCount, Func<long, double> sizeToLoadFactor)
		{
			_random = random;
			_initCapacity = PrimeNumber.Next(initCapacity - 1);
			_sizeToLoadFactor = sizeToLoadFactor;
			_getsCount = getsCount;
		}

		public void Init(long size)
		{
			Console.Write("Initializing (D Get) load factor {0}... ", _sizeToLoadFactor(size));

			_dictionary = new Dictionary<int, string>(_initCapacity);
			var initCount = (int)(_sizeToLoadFactor(size) * _initCapacity);
			var keys = new KeysWithoutDuplication(_random).Generate(initCount);
			var text = "init string";
			for (var i = 0; i < initCount; i++)
			{
				_dictionary.Add(keys[i], text);
			}

			Console.WriteLine("Testing...");
		}

		public void Execute()
		{
			string result;
			for (int i = 0; i < _getsCount; i++)
			{
				_dictionary.TryGetValue(_random.Next(), out result);
			}
		}
	}
}
