﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Hecsit.Algorithms.Lab5.Benchmarkable
{
	class Program
	{
		static void Main(string[] args)
		{
			var random = new Random();
			var items = new List<IBenchmarkable>();
			int capacity = 1000000;
			int operationsCount = 50000;
			Func<long, double> del = x => x / 100.0;
			items.Add(new HashTablePutBenchmarkable(random, capacity, operationsCount, del));
			items.Add(new DictionaryAddBenchmarkable(random, capacity, operationsCount, del));
			items.Add(new HashTableGetValueBenchmarkable(random, capacity, operationsCount, del));
			items.Add(new DictionaryGetValueBenchmarkable(random, capacity, operationsCount, del));

			var measurements = Benchmark.Perform(items, 5, 65, 12, 10);
			using (var file = File.CreateText("table.csv"))
			{
				file.WriteLine("load, %;HT Put;D Add;HT Get;D Get");
				foreach (var measurement in measurements)
				{
					file.WriteLine(measurement);
				}
			}
			Console.WriteLine("Finished");
			Console.ReadLine();
		}
	}
}
