﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Hecsit.Algorithms.Lab2
{
    class Program
    {
		static void Main(string[] args)
        {
			Console.Write("\n Enter start N:  ");
			long startN = long.Parse(Console.ReadLine());
			Console.Write("\n Enter end N:  ");
			long endN = long.Parse(Console.ReadLine());
			Console.Write("\n Enter steps number:  ");
			int steps = int.Parse(Console.ReadLine());
			Console.Write("\n Enter repetitions number:  ");
			int repetitions = int.Parse(Console.ReadLine());

			var benchmarkables = new List<IBenchmarkable>();
			benchmarkables.Add(new KBasedRecursiveBenchmarkable(8));
			benchmarkables.Add(new KBasedIterativeBenchmarkable(8));

			//using (var file = File.CreateText("table1.csv"))
			//{
			//    file.WriteLine("N;Recursive;Iterative");
			//    foreach (var measurement in Benchmark.Perform(benchmarkables, startN, endN, steps, repetitions))
			//    {
			//        file.WriteLine(measurement);
			//    }
			//}

			benchmarkables.RemoveAt(0);
			//Console.Write("\n Enter start N:  ");
			//startN = long.Parse(Console.ReadLine());
			//Console.Write("\n Enter end N:  ");
			//endN = long.Parse(Console.ReadLine());
			//Console.Write("\n Enter steps number:  ");
			//steps = int.Parse(Console.ReadLine());
			//Console.Write("\n Enter repetitions number:  ");
			//repetitions = int.Parse(Console.ReadLine());

			using (var file = File.CreateText("table2.csv"))
			{
				file.WriteLine("N;Time");
				foreach (var measurement in Benchmark.Perform(benchmarkables, startN, endN, steps, repetitions))
				{
					file.WriteLine(measurement);
				}
			}

			Console.WriteLine("\n Program finished");
            Console.ReadLine();
        }
    }
}
