﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hecsit.Algorithms.Lab2
{
    public class KBasedNumber
    {
        public uint Basis { get; private set; }

        public KBasedNumber(uint basis)
        {
            this.Basis = basis;
        }

        public ulong GetNDigitNumbersQuantityRecursive(long n)
        {
			if (n > 0)
			{
				switch (n)
				{
					case 1:
						return this.Basis - 1;
					case 2:
						return (this.Basis - 1) * this.Basis;
					default:
						return
							((this.Basis - 1) * this.GetNDigitNumbersQuantityRecursive(n - 1)
							+ (this.Basis - 1) * this.GetNDigitNumbersQuantityRecursive(n - 2));
				}
			}
			return 1;
        }

        public ulong GetNDigitNumbersQuantityIterative(long n)
        {
            ulong prePrevious = this.Basis - 1;
            ulong previous = this.Basis * (this.Basis - 1);
            ulong current = 0;

            for (uint i = 2; i < n; i++)
            {
                current = (previous + prePrevious) * (this.Basis - 1);
                prePrevious = previous;
                previous = current;
            }

            return current;
        }
    }
}
