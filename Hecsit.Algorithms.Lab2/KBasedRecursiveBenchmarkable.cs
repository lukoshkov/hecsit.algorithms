﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hecsit.Algorithms;

namespace Hecsit.Algorithms.Lab2
{
	public class KBasedRecursiveBenchmarkable : IBenchmarkable
	{
		private KBasedNumber _number;

		private uint Basis { get; set; }

		private long Size { get; set; }

		public KBasedRecursiveBenchmarkable(uint basis)
		{
			Basis = basis;
		}

		public void Init(long size)
		{
			_number = new KBasedNumber(Basis);
			Size = size;
		}

		public void Execute()
		{
			_number.GetNDigitNumbersQuantityRecursive(Size);
		}
	}
}
