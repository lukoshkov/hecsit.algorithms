﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab6
{
	public class Department
	{
		private readonly int _universityCode;
		private readonly int _facultyCode;
		private readonly int _departmentCode;

		public int UniversityCode
		{
			get
			{
				return _universityCode;
			}
		}

		public int FacultyCode
		{
			get
			{
				return _facultyCode;
			}
		}

		public int DepartmentCode
		{
			get
			{
				return _departmentCode;
			}
		}

		public Department(int universityCode, int facultyCode, int departmentCode)
		{
			_universityCode = universityCode;
			_facultyCode = facultyCode;
			_departmentCode = departmentCode;
		}
	}
}
