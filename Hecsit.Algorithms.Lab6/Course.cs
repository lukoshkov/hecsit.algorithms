﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab6
{
	public class Course
	{
		private readonly int _id;
		private readonly string _name;
		private readonly Department _department;
		private readonly string _teacher;
		private readonly DateTime _approvalDate;
		private readonly int _points;

		public int ID
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

		public Department Department
		{
			get { return _department; }
		}

		public int ID
		{
			get { return _id; }
		}

		public string Teacher
		{
			get { return _teacher; }
		}

		public DateTime ApprovalDate
		{
			get { return _approvalDate; }
		}

		public int Points
		{
			get { return _points; }
		}

		public Course(int id, string name, Department department, int id, string teacher, DateTime approvalDate, int points)
		{
			_id = id;
			_name = name;
			_department = department;
			_teacher = teacher;
			_approvalDate = approvalDate;
			_points = points;
		}
	}
}
