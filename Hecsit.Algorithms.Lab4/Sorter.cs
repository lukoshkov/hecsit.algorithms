﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab4
{
	public class Sorter
	{
		#region uselesscode

		public static void InsertionSort<T>(IList<T> collection) where T : IComparable<T>
		{
			InsertionSort(collection, 0, collection.Count - 1);
		}

		public static void QuickSort<T>(IList<T> collection) where T : IComparable<T>
		{
			QuickSort(collection, 0, collection.Count - 1);
		}

		public static void HeapSort<T>(IList<T> collection) where T : IComparable<T>
		{
			int right = collection.Count - 1;
			int left = 0;
			HeapSort(collection, left, right);
		}

		public static void HeapSort<T>(IList<T> collection, int left, int right) where T : IComparable<T>
		{
			MaxHeap<T>.Build(collection, left, right);

			for (int i = right; i > left; i--)
			{
				var buffer = collection[left];
				collection[left] = collection[i];
				collection[i] = buffer;
				MaxHeap<T>.SiftDown(collection, 0, left, i);
			}
		}

		#endregion

		public static IList<int> CountingSort(IList<int> collection, int minValue, int maxValue)
		{
			int k = maxValue - minValue + 1;
			var counts = new int[k];
			for (int j = 0; j < k; j++)
			{
				counts[j] = 0;
			}
			for (int i = 0; i < collection.Count; i++)
			{
				counts[collection[i] - minValue] += 1;
			}
			for (int j = 1; j < k; j++)
			{
				counts[j] = counts[j - 1] + counts[j];
			}

			var result = new int[collection.Count];
			for (int i = collection.Count - 1; i >= 0; i--)
			{
				result[counts[collection[i] - minValue] - 1] = collection[i];
				counts[collection[i] - minValue] -= 1;
			}
			return result;
		}

		#region otheruselesscode

		private static void InsertionSort<T>(IList<T> collection, int left, int right) where T : IComparable<T>
		{
			for (int i = left + 1; i <= right; i++)
			{
				var j = i;
				while (j > left && collection[j].CompareTo(collection[j - 1]) == -1)
				{
					var buffer = collection[j];
					collection[j] = collection[j - 1];
					collection[j - 1] = buffer;
					j--;
				}
			}
		}

		private static void QuickSort<T>(IList<T> collection, int left, int right) where T : IComparable<T>
		{
			if (right - left > 32)
			{
				int i = left;
				int j = right;
				var random = new Random();
				var pivot = collection[random.Next(left, right)];

				while (i <= j)
				{
					while (collection[i].CompareTo(pivot) < 0)
					{
						i++;
					}

					while (collection[j].CompareTo(pivot) > 0)
					{
						j--;
					}

					if (i <= j)
					{
						var buffer = collection[i];
						collection[i] = collection[j];
						collection[j] = buffer;

						i++;
						j--;
					}
				}

				if (left < j)
				{
					QuickSort(collection, left, j);
				}

				if (i < right)
				{
					QuickSort(collection, i, right);
				}
			}
			else
			{
				InsertionSort(collection, left, right);
			}
		}

		#endregion
	}
}
