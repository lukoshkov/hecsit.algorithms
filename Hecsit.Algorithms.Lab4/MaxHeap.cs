﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab4
{
    public class MaxHeap<T> where T : IComparable<T>
	{
		public static void Build(IList<T> collection)
		{
			int size = collection.Count;
			for (int i = size / 2 - 1; i >= 0; i--)
			{
				SiftDown(collection, i);
			}
		}

		public static void SiftDown(IList<T> collection, int i)
		{
			int l = Left(i);
			int r = Right(i);
			int size = collection.Count;
			int j = i;

			if (l < size && collection[j].CompareTo(collection[l]) < 0)
			{
				j = l;

				if (r < size && collection[j].CompareTo(collection[r]) < 0)
				{
					j = r;
				}
			}

			if (j != i)
			{
				var buffer = collection[i];
				collection[i] = collection[j];
				collection[j] = buffer;
				SiftDown(collection, j);
			}
		}

		public static void Build(IList<T> collection, int left, int right)
		{
			int size = Size(left, right);
			for (int i = size / 2 - 1; i >= 0; i--)
			{
				SiftDown(collection, i, left, right);
			}
		}

		public static void SiftDown(IList<T> collection, int i, int left, int right)
		{
			int l = Left(i);
			int r = Right(i);
			int size = Size(left, right);
			int j = i;
			
			if (l < size && collection[j + left].CompareTo(collection[l + left]) < 0)
			{
				j = l;

				if (r < size && collection[j + left].CompareTo(collection[r + left]) < 0)
				{
					j = r;
				}
			}

			if (j != i)
			{
				var buffer = collection[i + left];
				collection[i + left] = collection[j + left];
				collection[j + left] = buffer;
				SiftDown(collection, j, left, right);
			}
		}

		private static int Left(int i)
		{
			return 2 * i + 1;
		}

		private static int Right(int i)
		{
			return 2 * i + 2;
		}

		private static int Size(int left, int right)
		{
			return right - left + 1;
		}
    }
}
