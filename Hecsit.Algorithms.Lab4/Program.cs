﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Hecsit.Algorithms.Lab4
{
	class Program
	{
		static void Main(string[] args)
		{
			//var mas = new int[20];
			//var rand = new Random();
			//for (int i = 0; i < mas.Length; i++)
			//{
			//    mas[i] = rand.Next(100);
			//    Console.Write("{0}  ", mas[i]);
			//}
			//Console.WriteLine();
			//var sorted = Sorter.CountingSort(mas, 0, 100);
			//for (int i = 0; i < sorted.Count; i++)
			//{
			//    Console.Write("{0}  ", sorted[i]);
			//}

			var random = new Random();
			var algorithms = new List<IBenchmarkable>();
			algorithms.Add(new SorterBenchmarkable(random, 1000, 2000));
			algorithms.Add(new ArraySortBenchmarkable(random, 1000, 2000));
			var measurements = Benchmark.Perform(algorithms, 100000, 2100000, 20, 10);
			using (var file = File.CreateText("table.csv"))
			{
				file.WriteLine("N;CountingSort;Array.Sort");
				foreach (var measurement in measurements)
				{
					file.WriteLine(measurement);
				}
			}

			Console.WriteLine("Finished");

			Console.ReadLine();
		}
	}
}
