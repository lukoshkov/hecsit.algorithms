﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms;

namespace Hecsit.Algorithms.Lab4
{
	public class SorterBenchmarkable : IBenchmarkable
	{
		private int[] _array;
		private readonly Random _random;
		private readonly int _minValue;
		private readonly int _maxValue;

		public SorterBenchmarkable(Random random, int minValue, int maxValue)
		{
			_random = random;
			_maxValue = maxValue;
			_minValue = minValue;
		}

		public void Init(long size)
		{
			Console.Write("Initializing {0}...", size);
			_array = new int[size];
			for (int i = 0; i < size; i++)
			{
				_array[i] = _random.Next(_minValue, _maxValue);
			}
			Console.WriteLine(" Sorting...");
		}

		public void Execute()
		{
			Sorter.CountingSort(_array, _minValue, _maxValue);
		}
	}
}
