﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms;
using Hecsit.Algorithms.Lab3.Benchmarkables;
using System.IO;

namespace Hecsit.Algorithms.Lab3
{
	class Program
	{
		static void Main(string[] args)
		{
			var random = new Random();
			int minN = 100000;
			int maxN = 50000000;
			int repetitions = 10000;
			var queue = new PriorityQueue<int, int>(QueueType.Max, maxN + repetitions);
			queue.Reducible = false;

			var benchmarkables = new List<IBenchmarkable>();
			benchmarkables.Add(new PeekBenchmarkable(queue, random, repetitions));
			benchmarkables.Add(new EnqueueBenchmarkable(queue, random, repetitions));
			benchmarkables.Add(new DequeueBenchmarkable(queue, random, repetitions));
			benchmarkables.Add(new UpdatePriorityBenchmarkable(queue, random, repetitions));

			var measurements = Benchmark.Perform(benchmarkables, minN, maxN, 19, 10);
			using (var file = File.CreateText("table.csv"))
			{
				file.WriteLine("N;Peek;Enqueue;Dequeue;Update Priority");
				foreach (var measurement in measurements)
				{
					file.WriteLine(measurement);
				}
			}

			Console.WriteLine("Finished");
			Console.ReadLine();
		}
	}
}
