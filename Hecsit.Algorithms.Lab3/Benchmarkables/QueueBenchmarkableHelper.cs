﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms.Lab3;

namespace Hecsit.Algorithms.Lab3.Benchmarkables
{
	public class QueueBenchmarkableHelper
	{
		public static void BringToSize(PriorityQueue<int, int> queue, Random random, int size)
		{
			var quantity = size - queue.Count;
			Console.Write(".");
			for (var i = 0; i < quantity; i++)
			{
				queue.Enqueue(random.Next(), random.Next());
			}
			for (var i = 0; i < -quantity; i++)
			{
				queue.Dequeue();
			}
		}
	}
}
