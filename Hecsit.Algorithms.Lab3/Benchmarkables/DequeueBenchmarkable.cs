﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hecsit.Algorithms;
using Hecsit.Algorithms.Lab3;

namespace Hecsit.Algorithms.Lab3.Benchmarkables
{
	public class DequeueBenchmarkable : IBenchmarkable
	{
		private readonly Random _random;
		private readonly PriorityQueue<int, int> _queue;
		private readonly int _repetitions;

		public DequeueBenchmarkable(PriorityQueue<int, int> queue, Random random, int repetitions)
		{
			_random = random;
			_repetitions = repetitions;
			_queue = queue;
		}

		public void Init(long size)
		{
			QueueBenchmarkableHelper.BringToSize(_queue, _random, (int)size);
		}

		public void Execute()
		{
			for (int i = 0; i < _repetitions; i++)
			{
				_queue.Dequeue();
			}
		}
	}
}
