﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab3
{
	public enum QueueType
	{
		Max,
		Min
	}

	public class PriorityQueue<TKey, TData> where TKey : IComparable<TKey>
	{
		private struct QueueItem : IComparable<QueueItem>
		{
			public TKey Key { get; set; }
			public TData Data { get; set; }
			public QueueCursor Cursor { get; set; }

			public int CompareTo(QueueItem other)
			{
				return Key.CompareTo(other.Key);
			}

			public static void Swap(ref QueueItem left, ref QueueItem right)
			{
				var key = left.Key;
				var data = left.Data;
				var cursor = left.Cursor;
				left.Key = right.Key;
				left.Data = right.Data;
				left.Cursor = right.Cursor;
				right.Key = key;
				right.Data = data;
				right.Cursor = cursor;
			}
		}

		private QueueItem[] _heap;
		private readonly int _compareResult;

		public bool Reducible { get; set; }
		public int Size { get; private set; }
		public int Count { get; private set; }

		public bool Empty
		{
			get { return Count == 0; }
		}

		public PriorityQueue(QueueType type, int size = 10)
		{
			Size = size;
			_heap = new QueueItem[Size];
			Count = 0;
			_compareResult = type == QueueType.Max ? -1 : 1;
			Reducible = true;
		}

		public QueueCursor Enqueue(TKey key, TData data)
		{
			if (Count + 1 == Size)
			{
				Size = Size * 2;
				var buffer = _heap;
				_heap = new QueueItem[Size];
				buffer.CopyTo(_heap, 0);
			}

			_heap[Count].Key = key;
			_heap[Count].Data = data;
			var cursor = new QueueCursor();
			_heap[Count].Cursor = cursor;
			Count = Count + 1;
			SiftUp(Count);
			return cursor;
		}

		public TData Dequeue()
		{
			if (Count > 0)
			{
				var result = _heap[0].Data;
				Count = Count > 0 ? Count - 1 : 0;
				_heap[0] = _heap[Count];
				SiftDown(1);
				if (Count < Size / 4  && Reducible)
				{
					var buffer = _heap;
					Size = Size / 2;
					_heap = new QueueItem[Size];
					for (int i = 0; i < Count; i++)
					{
						_heap[i] = buffer[i];
					}
				}
				return result;
			}
			else
			{
				throw new InvalidOperationException("Queue is empty");
			}
		}

		private void SiftDown(int i)
		{
			int left = Left(i);
			int right = Right(i);
			int j = i;

			if (left <= Count && _heap[j - 1].CompareTo(_heap[left - 1]) == _compareResult)
			{
				j = left;
			}

			if (right <= Count && _heap[j - 1].CompareTo(_heap[right - 1]) == _compareResult)
			{
				j = right;
			}

			if (j != i)
			{
				QueueItem.Swap(ref _heap[i - 1], ref _heap[j - 1]);
				SiftDown(j);
			}
		}

		private void SiftUp(int i)
		{
			int parent = Parent(i);

			while (i > 1 && _heap[i - 1].CompareTo(_heap[parent - 1]) != _compareResult)
			{
				QueueItem.Swap(ref _heap[i - 1], ref _heap[parent - 1]);
				i = Parent(i);
				parent = Parent(i);
			}
		}

		public TData Peek()
		{
			if (Count > 0)
			{
				return _heap[0].Data;
			}
			else
			{
				throw new InvalidOperationException("Queue is empty");
			}
		}

		public void Clear()
		{
			Count = 0;
		}

		public void UpdatePriority(QueueCursor cursor, TKey key)
		{
			int index = 0;
			while (index < Count && _heap[index].Cursor != cursor)
			{
				index = index + 1;
			}
			if (index < Count)
			{
				var oldKey = _heap[index].Key;
				_heap[index].Key = key;
				if (key.CompareTo(oldKey) == _compareResult)
				{
					SiftDown(index + 1);
				}
				else
				{
					SiftUp(index + 1);
				}
			}
		}

		private int Left(int i)
		{
			return 2 * i;
		}

		private int Right(int i)
		{
			return 2 * i + 1;
		}

		private int Parent(int i)
		{
			return i / 2;
		}
	}
}
