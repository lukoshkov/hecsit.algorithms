﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms
{
    public interface IBenchmarkable
    {
        void Init(long size);
        void Execute();
    }
}
