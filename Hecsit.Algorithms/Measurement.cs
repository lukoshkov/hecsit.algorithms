﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms
{
    public class Measurement
    {
        public long Size { get; private set; }

        public List<decimal> Time { get; private set; }

        public Measurement(long size, List<decimal> time)
        {
			Time = new List<decimal>();
            Size = size;
			foreach (var item in time)
			{
				Time.Add(item);
			}
        }

		public override string ToString()
		{
			var result = new StringBuilder();
			result.Append(Size);
			foreach (var item in Time)
			{
				result.Append(";");
				result.Append(item);
			}
			return result.ToString();
		}
    }
}
