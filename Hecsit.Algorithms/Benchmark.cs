﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Hecsit.Algorithms
{
    public class Benchmark
    {
        private static readonly int _wrongResultPercent = 20;

		private Benchmark()
		{
		}

        private static long Test(IBenchmarkable item)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            item.Execute();
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }

        private static decimal CalculateAverage(long[] collection)
        {
            Array.Sort(collection);
            decimal result = 0;
            int quantity = collection.Length * _wrongResultPercent / 100;
            for (int i = 0; i < quantity; i++)
            {
                result += collection[i];
            }
            return result / quantity;
        }

        public static List<Measurement> Perform(List<IBenchmarkable> items, long startSize, long endSize, int stepsNumber, int repetitionsNumber)
        {
            var measurements = new List<Measurement>();
            long increment = (endSize - startSize) / stepsNumber;
			for (long size = startSize; size <= endSize; size += increment)
			{
				var results = new List<decimal>();
				foreach (var item in items)
				{
					var time = new long[repetitionsNumber];
					for (int i = 0; i < repetitionsNumber; i++)
					{
						item.Init(size);
						time[i] = Test(item);
					}
					results.Add(CalculateAverage(time));
				}
				measurements.Add(new Measurement(size, results));
			}
            return measurements;
        }
    }
}
