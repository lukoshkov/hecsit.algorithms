﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab5.Core
{
	public class PrimeNumber
	{
		public static int Next(int number)
		{
			number = number + 1;
			while (!IsPrime(number))
			{
				number = number + 1;
			}
			return number;
		}

		public static bool IsPrime(int number)
		{
			var sqrt = Math.Sqrt(number);
			if (number % 2 == 0 && number != 2)
			{
				return false;
			}
			for (int i = 3; i <= sqrt; i += 2)
			{
				if (number % i == 0)
				{
					return false;
				}
			}
			return true;
		}
	}
}
