﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hecsit.Algorithms.Lab5.Core
{
	public class HashTable<TKey, TValue>
	{
		#region subtypes

		private enum ItemStatus
		{
			Empty,
			Deleted,
			Valid
		}

		private struct TableItem
		{
			public TKey Key { get; set; }
			public TValue Value { get; set; }
			public ItemStatus Status { get; set; }
		}

		private class TKeyEqualityComparer : IEqualityComparer<TKey>
		{
			public bool Equals(TKey x, TKey y)
			{
				return x.Equals(y);
			}

			public int GetHashCode(TKey obj)
			{
				return obj.GetHashCode();
			}
		}

		#endregion

		private static readonly double _maxLoadFactor = 0.7;
		private static readonly double _minLoadFactor = 0.01;
		private static readonly int _defaultCapacity = 11;
		private TableItem[] _table;
		private IEqualityComparer<TKey> _comparer;

		public int Count { get; private set; }

		public int Capacity
		{
			get
			{
				return _table.Length;
			}
		}

		private double LoadFactor
		{
			get
			{
				return (double)Count / _table.Length;
			}
		}

		#region ctors

		public HashTable()
			: this(null)
		{
		}

		public HashTable(IEqualityComparer<TKey> comparer)
			: this(_defaultCapacity, comparer)
		{
		}

		public HashTable(int capacity)
			: this(capacity, null)
		{
		}

		public HashTable(int capacity, IEqualityComparer<TKey> comparer)
		{
			InitTable(capacity);
			if (comparer != null)
			{
				_comparer = comparer;
			}
			else
			{
				_comparer = new TKeyEqualityComparer();
			}
		}

		#endregion

		#region public methods

		public void Put(TKey key, TValue value)
		{
			if (LoadFactor > _maxLoadFactor)
			{
				Rehash(_table.Length * 2);
			}

			int probe = 0;
			var hashCode = _comparer.GetHashCode(key);
			while (probe < _table.Length)
			{
				var index = GetHash(hashCode, probe);
				if (_table[index].Status == ItemStatus.Empty || _table[index].Status == ItemStatus.Deleted)
				{
					_table[index].Value = value;
					_table[index].Key = key;
					_table[index].Status = ItemStatus.Valid;
					Count = Count + 1;
					return;
				}
				probe = probe + 1;
			}
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			value = default(TValue);
			var index = Find(key);
			if (index != -1)
			{
				value = _table[index].Value;
				return true;
			}
			return false;
		}

		public void Remove(TKey key)
		{
			var index = Find(key);
			if (index != -1)
			{
				_table[index].Status = ItemStatus.Deleted;
				Count = Count - 1;
			}
			if (LoadFactor < _minLoadFactor && Capacity >= 2 * _defaultCapacity)
			{
				Rehash(Capacity / 2);
			}
		}

		public void Clear()
		{
			InitTable(_defaultCapacity);
		}

		#endregion

		#region private methods

		private int Find(TKey key)
		{
			int probe = 0;
			var hashCode = _comparer.GetHashCode(key);
			while (probe < _table.Length)
			{
				var index = GetHash(hashCode, probe);
				if (_table[index].Status == ItemStatus.Empty)
				{
					return -1;
				}

				if (_table[index].Status == ItemStatus.Valid && _comparer.Equals(_table[index].Key, key))
				{
					return index;
				}

				probe = probe + 1;
			}
			return -1;
		}

		private int GetHash(int key, int probe)
		{
			return (GetHash1(key) + probe * GetHash2(key)) % _table.Length;
		}

		private int GetHash1(int key)
		{
			return key % _table.Length;
		}

		private int GetHash2(int probe)
		{
			return 1 + probe % (_table.Length - 1);
		}

		private void Rehash(int capacity)
		{
			var temp = _table;
			InitTable(capacity);
			var items = temp.Where(x => x.Status == ItemStatus.Valid);
			foreach (var item in items)
			{
				Put(item.Key, item.Value);
			}
		}

		private void InitTable(int capacity)
		{
			capacity = PrimeNumber.Next(capacity - 1);
			_table = new TableItem[capacity];
			for (int i = 0; i < _table.Length; i++)
			{
				_table[i].Status = ItemStatus.Empty;
			}
			Count = 0;
		}

		#endregion
	}
}
